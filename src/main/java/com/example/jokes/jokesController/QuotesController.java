package com.example.jokes.jokesController;

import com.example.jokes.model.Quotes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class QuotesController {

        Quotes quotes = new Quotes();
    @GetMapping("/jokes")
    public String Quotes(ModelMap modelMap){

        modelMap.put("jokes", quotes.getRandomQuote());
        return "index";
    }
}
